#!/usr/bin/env python
# This script can be run by the following (versions can be changed):
# - Setup runtime environment for relevant Gauss version, e.g.
# lb-run Gauss/v56r3 bash
# - Set environmental variable BCVEGPYDATAROOT to point to your local copy
# - From BcVegPyData/doc run
#    ./grids.py --config $GAUSSOPTS/Gauss-2012.py --output 8TeV
# Warning, with the default configuration, grid generation can take
# some time, e.g. two days.
import os, shutil, argparse

# Parse command options.
parser = argparse.ArgumentParser(
    description = "Script to generate BcVegPy grid files.")
parser.add_argument("-c", "--config", type = str,
                    default = "$GAUSSOPTS/Gauss-2012.py",
                    help = "the beam configuration file")
parser.add_argument("-o", "--output", type = str,
                    default = "8TeV",
                    help = "the output directory")
parser.add_argument("-t", "--test", action = "store_true",
                    help = "flag to run a fast test run")
args = parser.parse_args()

# Copy dummy grid files to the output.
if not os.path.isdir("../data/" + args.output):
    shutil.copytree("../data/7TeV", "../data/" + args.output)

# Create the configuration file.
opts = [
    "# Import the necessary options.",
    "from Gaudi.Configuration import importOptions",
    "importOptions('$GAUSSOPTS/GenStandAlone.py')",
    "importOptions('$DECFILESROOT/options/14103001.py')",
    "importOptions('$LBPYTHIA8ROOT/options/Pythia8.py')",
    "importOptions('$LBBCVEGPYROOT/options/BcVegPyPythia8.py')",
    "importOptions('$GAUSSOPTS/FixedNInteractions.py')",
    "importOptions(%r)" % args.config,
    "",
    "# Configure Gauss.",
    "from Gauss.Configuration import GenInit, LHCbApp",
    "GaussGen = GenInit('GaussGen')",
    "GaussGen.FirstEventNumber = 1",
    "GaussGen.RunNumber = 1",
    "nEvts = 1",
    "LHCbApp().EvtMax = 1",
    "",
    "# Add BcVegPy as special production tool and instruct to build grids.",
    "from Configurables import Generation, Special, BcVegPyProduction",
    "Generation().addTool(Special)",
    "Generation().Special.ProductionTool = 'BcVegPyProduction'",
    "Generation().Special.addTool(BcVegPyProduction)",
    "Generation().Special.BcVegPyProduction.Commands += [",
    "    'loggrade ivegasopen 1',"
]
if args.test: opts += [
        "    'vegasinf number 10', # Number of sampling points.",
        "    'vegasinf nitmx 20',  # Maximum number of iterations."
]
opts += ["]"]
cfg = open("cfg.py", "w")
for opt in opts: cfg.write(opt + "\n")
cfg.close()
    
# Run the command to create the grids in place.
cmd = "gaudirun.py cfg.py"
os.system(cmd)
