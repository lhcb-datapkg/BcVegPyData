# BcVegPyData package

This package contains grid files for BcVegPy event generators needed for productions. Separate grid files for each beam energy are required.

## Generating new grid files

The grid files are generated directly by the BcVegPy event generator during initialisation phase. It requires special options to be set. To facilitate this, script `scripts/grids.py` is provided. Usage to generate grid files is:
```bash
# Get local copy of BcVegPyData package
git clone ssh://git@gitlab.cern.ch:7999/lhcb-datapkg/BcVegPyData.git
cd BcVegPyData
git checkout -b <your new branch>
# Setup Gauss runtime environment (released Sim10 version in this case)
lb-run Gauss/v56r3 bash
# Point Gauss to your local copy of BcVegPyData
export BCVEGPYDATAROOT=<path to your local copy>
# Go to BcVegPyData/scripts directory
cd scripts
# Run grid generation
# Recommend first trying with -t option for quick test that everything is setup properly
# Example here generates grids for 8 TeV, change Gauss-2012.py to appropriate year
./grids.py --config $GAUSSOPTS/Gauss-2012.py --output 8TeV
```
Afterwards commit, push branch to gitlab and make merge request.

Note, before BcVegPy generator can be used for new energy, files `Gen/BcVegPy/src/bcvegpy_upinit.F` and `Gen/BcVegPy/src/bcvegpy_upevnt.F` in Gauss project need to be updated to recognize new energy and translate it to correct subdirectory in BecVegPyData package.
